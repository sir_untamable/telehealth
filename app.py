#!/home/root/todo-api/flask/bin/python
from flask import Flask, jsonify
from flask import render_template
import sqlite3

tasks = []
app = Flask(__name__,static_url_path="/static")

def updatetsk():
	del tasks[:]
	conn = sqlite3.connect('TeleHealthDB.db')
	curs = conn.cursor()
	for row in curs.execute("SELECT * FROM demo"):
		tasks.append({'Timestamp': row[0], 'Device ID': row[1], 'Patient Name': row[2], 'Patient Age': row[3], 'Mobile': row[4], 'Email' : row[5], 'Address': row[6], 'Temperature': row[7], 'SPO2': row[8], 'Pulse Rate': row[9], 'Body Orientation': row[10], 'Breathe' : row[11], 'Comments': row[12] })
	conn.close()
	return tasks

@app.route('/TeleHealth', methods = ['GET'])
def get_tasks():
	updatetsk()
	return jsonify({'tasks' : tasks})

@app.route('/viewAllData', methods = ['GET'])
def get_tasks_html():
	updatetsk()
	return render_template('index.html',tasks= tasks,test=1)


@app.route('/TeleHealthRealTime', methods = ['GET'])
def TeleHealthRealTime():
	connc = sqlite3.connect('TeleHealthDB.db')
        curss = connc.cursor()
	curss.execute("SELECT * FROM demo ORDER BY datetime DESC LIMIT -1;")
	rows = curss.fetchall()
	row = rows[-1]
	task = ({'Timestamp': row[0], 'Device ID': row[1], 'Patient Name': row[2], 'Patient Age': row[3], 'Mobile': row[4], 'Email' : row[5], 'Address': row[6], 'Temperature': row[7], 'SPO2': row[8], 'Pulse Rate': row[9], 'Body Orientation': row[10], 'Breathe' : row[11], 'Comments': row[12] })
        connc.close()
        return jsonify({'data' : task})

@app.route('/viewLastData', methods = ['GET'])
def TeleHealthRealTime_html():
	connc = sqlite3.connect('TeleHealthDB.db')
        curss = connc.cursor()
	curss.execute("SELECT * FROM demo ORDER BY datetime DESC LIMIT -1;")
	rows = curss.fetchall()
	row = rows[-1]
	task = ({'Timestamp': row[0], 'Device ID': row[1], 'Patient Name': row[2], 'Patient Age': row[3], 'Mobile': row[4], 'Email' : row[5], 'Address': row[6], 'Temperature': row[7], 'SPO2': row[8], 'Pulse Rate': row[9], 'Body Orientation': row[10], 'Breathe' : row[11], 'Comments': row[12] })
        connc.close()
        return render_template('individual.html',task= task)



if __name__ == '__main__':
	app.run(debug = True, host = '0.0.0.0')

