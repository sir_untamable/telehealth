import paho.mqtt.client as mqtt
import sqlite3
import datetime

conn = sqlite3.connect('TeleHealthDB.db')
curs = conn.cursor()

def on_connect(client, userdata, rc):
	print "Connected with result code " + str(rc)
	client.subscribe("TeleHealth")

def on_message(client, userdata, msg):
	print "Topic: ", msg.topic + '\nMessage: ' + str(msg.payload)
	stringg = "INSERT INTO demo values('%s','DCB2103', 'John Doe',38, 8728064862, 'johndoe@email.com', '320 S East St, Chicago, IL 60616', 24.5, 010, 001, '%s', '%s', '%s')" % (datetime.datetime.now().isoformat(' ')[:-7], msg.payload.split()[0], msg.payload.split()[1], msg.payload.split()[2])
	curs.execute(stringg)
	conn.commit()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("sufiankaki.me", 1883, 60)

client.loop_forever()
